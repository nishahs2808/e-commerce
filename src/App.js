import React,{useEffect,useState} from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import MenuItem from './components/menuItem/menuItem'
import Directory from './components/directory/directory';
import Shop from './components/shop/shop'
import shopPage from './pages/shopPage';
import homePage from './pages/homePage';
import signIn from './components/sign-up-sign-in/signIn';
import signInSignUp from './components/sign-up-sign-in/signInSignUp';
import {createUserProfile, auth} from './firebase/firebase.utils';
import Header from './components/header/Header';
function App() {
	const [user, setUser] = useState("");
	const unSubscribeFromAuth =null;
	useEffect(() => {
		auth.unSubscribeFromAuth = auth.onAuthStateChanged(async (user) => {
			if(user){
				const userRef = await createUserProfile(user);
				console.log(userRef)
				userRef.onSnapshot(snapShot => {
					setUser(
						{
							user:{
								id:snapShot.id,
								...snapShot.data()
							}
						}
					)
				})
			}else{
				setUser({ user: user })
			}
			
			createUserProfile(user)
			console.log("user",user)
		})
		return () => {
			unSubscribeFromAuth();
		}
	}, [])
	return (
		<BrowserRouter>
			<Header currentUser={user}></Header>
			<Switch>
				<Route exact path="/" component={homePage}></Route>
				<Route path="/shop" component={shopPage}></Route>
				<Route path="/signup" component={signIn}></Route>
				<Route path="/signInSignUp" component={signInSignUp}></Route>
			</Switch>
		</BrowserRouter>
	);
}

export default App;
