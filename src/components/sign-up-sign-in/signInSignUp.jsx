import React from 'react'
import SignIn from './signIn';
import SignUp from './signUp';
import {Grid} from "@material-ui/core";
import './signInSignUp.css';
function signInSignUp() {
	return (
		<div style={{width:"100%"}}>
			<Grid container>
				<Grid item lg={6}>
					<SignIn></SignIn>
				</Grid>
				<Grid item lg={6}>
					<SignUp></SignUp>
				</Grid>
			</Grid>
		</div>
	)
}

export default signInSignUp
