import React,{useState} from 'react';
import { Typography,Button, Grid } from '@material-ui/core'
import FormInput from '../FormInput/FormInput';
import {auth,signInWithGoogle} from '../../firebase/firebase.utils';
const SignIn =()=>{
	const [email, setEmail] = useState('');
	const [password,setPassword] =useState('');
	const handleSubmit = async() => {
		try{
			await auth.signInWithEmailAndPassword(email, password);
			setEmail('');
			setPassword('');
		}catch(error){
			console.log(error);
			
		}
	}

	return (
		<div style={{width:"100%",textAlign:"center"}}>
			<Grid container direction="row"
				justify="center" alignItems="center" spacing={2} >
		      <Grid item lg={10} md={10} xs={10} >
					<Typography>I already have an account</Typography>

			  </Grid>
				<Grid item lg={10} md={10} xs={10}>
					<Typography>Sign in with your email and password</Typography>

				</Grid>
				<Grid item lg={10} md={10} xs={10}>
					<FormInput label='email' type="email" value={email} setValue={setEmail}></FormInput>

				</Grid>
				<Grid item lg={10} md={10} xs={10}>
					<FormInput label='password' type='password' value={password} setValue={setPassword}></FormInput>

				</Grid>
				<Grid item lg={10} md={10} xs={10}>
					<Button variant="contained" color="primary" onClick={handleSubmit}>SIGN IN</Button>
					<Button variant="contained" color="primary" onClick={signInWithGoogle}>SIGN IN WITH GOOGLE</Button>

				</Grid>
				<Grid item lg={5} md={5} xs={5}>
					

				</Grid>
			
		 </Grid>
		</div>
	)
}

export default SignIn;
