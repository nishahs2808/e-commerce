import React, { useState } from 'react';
import { Grid, Typography, Button } from '@material-ui/core';
import FormInput from '../FormInput/FormInput';
import { auth, createUserProfile } from '../../firebase/firebase.utils';
const SignUp = () => {
	const [displayName, setDisplayName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	const handleSubmit =async () => {
		console.log("submiited",email);
		if (password !== confirmPassword) {
			alert("password doent match");
			return
		} else {
			try {
				console.log(auth)
				const { user } = await auth.createUserWithEmailAndPassword(email, password);
				await createUserProfile(user, { displayName });
				setEmail('');
				setPassword('');
				setPassword('');
				setConfirmPassword('');
			} catch (error) {
				console.log(error)
			}

		}
	}
	return (
		<div style={{ width: "100%", textAlign: "center" }}>
			<Grid container direction="row"
				justify="center" alignItems="center" spacing={2}>
				<Grid item lg={10} md={10} xs={10} >
					<Typography className="full-width">I dont have an account</Typography>

				</Grid>
				<Grid item lg={10} md={10} xs={10}>
					<Typography >Sign up with your email and password</Typography>

				</Grid>
				<Grid item lg={10} md={10} xs={10}>
					<FormInput label='Display Name' type="text" value={displayName} setValue={setDisplayName}></FormInput>

				</Grid>
				<Grid item lg={10} md={10} xs={10}>
					<FormInput label='email' type="email" value={email} setValue={setEmail}></FormInput>

				</Grid>
				<Grid item lg={10} md={10} xs={10}>
					<FormInput label='password' type='password' value={password} setValue={setPassword}></FormInput>

				</Grid>
				<Grid item lg={10} md={10} xs={10}>
					<FormInput label='Confirm Password' type="password" value={confirmPassword} setValue={setConfirmPassword}></FormInput>

				</Grid>
				<Grid item lg={10} md={10} xs={10}>
					<Button variant="contained" color="primary" onClick={handleSubmit}>SIGN UP</Button>

				</Grid>

			</Grid>
		</div>
	)
}

export default SignUp
