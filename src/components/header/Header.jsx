import React from 'react'
import { Link } from 'react-router-dom'
import { Typography } from '@material-ui/core'
const logo = require('../../assets/crown.svg');
function Header(props) {
	return (
		<div>
			<div>
				<Link to='/'>
					<img src={logo}></img>
				</Link>
			</div>
			<div>
				<Link to="/shop">
					<Typography>SHOP</Typography>
				</Link>
			</div>
			{
			props.user ?
			 <div>
				<Link>
					<Typography>LOGOUT</Typography>
				</Link>
			</div> :
			<>
				<div>
					<Link to="/signup">
						<Typography>SIGN UP</Typography>
					</Link>
				</div>
				<div>
					<Link to="/signInSignUp">
						<Typography>SIGN IN</Typography>
					</Link>
				</div>
			</>
			}

		</div>
	)
}

export default Header
