import React from "react";
import { Card, Paper, CardContent } from "@material-ui/core";
import logo from "../../assets/dummy.png";
import "./menuItem.css";
const menuItem = (props) => {
  return (
    <Paper
      style={{
        height: "30vh",
        backgroundImage: `url(${props.imageUrl})`,
        backgroundSize: "cover",
        display: "flex",
        justify: "center",
        alignItems: "center",
        margin: "10px",
		padding:"2px"
      }}
      className="menu-item"
    >
      <div
        style={{
          backgroundColor: "white",
          opacity: ".5",
          padding: "20px 20px",
        }}
      >
        <div>{props.title}</div>
      </div>
    </Paper>
  );
};

export default menuItem;
