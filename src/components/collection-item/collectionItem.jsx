import React from 'react'
import { Grid, Paper } from '@material-ui/core';
function collectionItem(props) {
	console.log("item", props)
	return (
		<Grid item lg={3} md={3} xs={5}>
	
			<div style={{
				backgroundColor: "white",
			width:"250px",
					boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
					transition: "0.3s"
				}}>
				<div
					style={{
						height: "40vh",
						backgroundImage: `url(${props.imageUrl})`,
						backgroundSize: "cover",
						display: "flex",
						justify: "center",
						alignItems: "center",
						margin: "10px",
						padding: "2px",
						display:"flex",
						justifyContent:"center",
						alignContent:"center"
					}}
				>
					<div style={{padding:"5px",backgroundColor:"white",opacity:".7"}}>ADD TO CART</div>
				</div>
				<div className="footer" style={{ display: "flex", justifyContent: "space-between",padding:"2px",margin:"10px" }}>
					<div >{props.name}</div>
					<div>${props.price}</div>
				</div>
			</div>
		
		</Grid>
	)
}

export default collectionItem
