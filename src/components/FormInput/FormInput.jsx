import React from 'react'
import {TextField} from "@material-ui/core";
function input(props) {
	return (
		<TextField label={props.type } name={props.name} value={props.value} onChange={e=>props.setValue(e.target.value)}/>
	)
}

export default input;
