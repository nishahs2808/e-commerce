import React from 'react'
import collections from '../../collection';
import CollectionPreview from '../collection-preview/collection-preview';
import {Grid} from '@material-ui/core';
function shop() {
	return (
		<Grid container>
		{
			collections.map((collection)=>(
				<>
				<CollectionPreview key={collection.id} {...collection}></CollectionPreview>
				</>
			))
		}
			
	
		</Grid>
	)
		
}

export default shop
