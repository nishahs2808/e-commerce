import React from 'react'
import CollectionItem from '../collection-item/collectionItem';
import { Grid } from '@material-ui/core';
function collectionPreview(props) {
	return (
		<Grid container style={{ marginTop: "10px" }} spacing={2} justify="space-evenly">
			<Grid lg={12} md={12} xs={12}>{props.title}</Grid>
			{props.items.map(item => <CollectionItem key={item.id} {...item}/>)}
			
		</Grid>
	)
}

export default collectionPreview
