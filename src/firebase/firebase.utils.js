import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
	apiKey: "AIzaSyAXcGlYhyy1jL1w6XWj3cYhLKUIzexoub0",
	authDomain: "crwn-db-9414c.firebaseapp.com",
	databaseURL: "https://crwn-db-9414c.firebaseio.com",
	projectId: "crwn-db-9414c",
	storageBucket: "crwn-db-9414c.appspot.com",
	messagingSenderId: "719216233104",
	appId: "1:719216233104:web:4081cda50951167bfca615",
	measurementId: "G-K4XK8J31L3"
}

export const createUserProfile = async (userAuth,additionalData)=>{
	console.log("auth", auth.createUserWithEmailAndPassword)
	if(!userAuth)
	return;
	
	const userRef = firestore.doc(`users/${userAuth.uid}`);
	const snapShot = await userRef.get();
	console.log("snapshot",snapShot);
	if(!snapShot.exists){
		try{
			const {displayName,email} = userAuth;
			const createdAt = new Date();
			await userRef.set({displayName,email,createdAt});
			console.log("auth", auth.createUserWithEmailAndPassword)
		}catch(error){
			console.log(error)
		}
	}
return userRef;
}

firebase.initializeApp(config);   



export const auth = firebase.auth();

export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();

provider.setCustomParameters({prompt:'select_account'});

export const signInWithGoogle = () => auth.signInWithPopup(provider);


export default firebase;